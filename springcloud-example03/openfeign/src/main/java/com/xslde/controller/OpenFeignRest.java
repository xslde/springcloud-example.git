package com.xslde.controller;

import com.xslde.openfeign.OpenFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by xslde on 2018/7/22
 */
@RestController
public class OpenFeignRest {

    @Autowired
    OpenFeignService openFeign;

    @GetMapping("/name")
    public String name(){
       return openFeign.test();
    }


}
