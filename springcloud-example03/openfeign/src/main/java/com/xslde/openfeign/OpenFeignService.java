package com.xslde.openfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by xslde on 2018/7/22
 */
@FeignClient(name = "eureka-client",fallback = OpenFeignFallbackServiceImpl.class)//eureka-client工程的服务名称
public interface OpenFeignService {

    @GetMapping("/name")//这里的请求路径需要和eureka-client中的请求路径一致
    public String test();//这里的方法名需要和eureka-client中的方法名一致

}
