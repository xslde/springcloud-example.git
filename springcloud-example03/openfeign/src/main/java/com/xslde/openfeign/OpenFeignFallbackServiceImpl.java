package com.xslde.openfeign;

import org.springframework.stereotype.Service;

/**
 * Created by xslde on 2018/7/22
 */
@Service
public class OpenFeignFallbackServiceImpl implements OpenFeignService{
    @Override
    public String test() {
        return "调用服务失败！";
    }
}
