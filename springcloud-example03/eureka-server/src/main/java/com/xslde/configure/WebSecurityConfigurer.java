package com.xslde.configure;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Created by xslde on 2018/7/21
 */
@EnableWebSecurity
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //为了支持http://xslde:123456@127.0.0.1:7100/eureka/这种认证方式，需启用httpBasic
        http.authorizeRequests().anyRequest().authenticated().and().csrf().disable().httpBasic();
    }

}
